# Romeo Courbis (romeo.courbis@sisprobe.com), June 2019
# Last updated Francois Lavoue, June 2020
# Copyright (c) 2019-2020, SISPROBE
# ----------------------------------------------------------------------

import h5py
from obspy import Trace, Stream, UTCDateTime
import copy
import os
import numpy as np


class SisDailyDatah5:
    """Handling daily h5 files made in Sisprobe"""

    def __init__(self, h5file):
        """
        Initialize a SisDatah5 object and getting sampling frequency, day to which data correspond
        and the list of stations.
        :param h5file: the filename of a h5 file.
        :type h5file: str
        """
        self.h5_file = h5file
        with h5py.File(h5file, 'r') as f:
            self.sampling_rate = f['_metadata']['fe'][()]
            self.day = UTCDateTime(f['_metadata']['t0_UNIX_timestamp'][()])
            # Getting full station names:
            self.station_list = dict()
            net_list = list(f.keys())
            net_list = [elt for elt in net_list if elt != '_metadata']
            for net in net_list:
                self.station_list[net] = dict()
                for sta in f[net].keys():
                    self.station_list[net][sta] = list(f[net][sta].keys())

    def get_data(self, net, sta, compo):
        """
        Get the data corresponding to a network, station and component.
        :param net:
        :type net: str
        :param sta:
        :type sta: str
        :param compo:
        :type compo: str
        :return: a 1D array
        :rtype:
        """
        with h5py.File(self.h5_file, 'r') as f:
            dset = "/" + net + "/" + sta + "/" + compo
            data = f[dset][:]
            return data

    def get_trace(self, net, sta, compo):
        """
        Return an obspy Trace object corresponding to a network, station, component and it's data.
        :param net:
        :param sta:
        :param compo:
        :return:
        """
        t = Trace(data=self.get_data(net, sta, compo))
        station = str(sta)
        station = station.split('.')
        sta = station[0]
        location = station[1]
        t.stats.network = net
        t.stats.station = sta
        t.stats.location = location
        t.stats.channel = compo
        t.stats.sampling_rate = self.sampling_rate
        t.stats.starttime = self.day

        return t

    def create_dataset_names(self, net_list=None, sta_list=None, compo_list=None, dset_name=False):
        """
        Create a selection of network(s), station(s) and component(s).
        :param net_list:
        :param sta_list:
        :param compo_list:
        :param dset_name:
        :return:
        """
        selected_stations = self.station_list
        remaining_stations = copy.deepcopy(selected_stations)

        # Selection of networks:
        if net_list is not None:
            for net in selected_stations.keys():
                if net not in net_list:
                    remaining_stations.pop(net)

        # Selection of stations:
        if sta_list is not None:
            for net in selected_stations.keys():
                for sta in selected_stations[net].keys():
                    if sta not in sta_list:
                        remaining_stations[net].pop(sta)

        # Selection of components:
        if compo_list is not None:
            for net in selected_stations.keys():
                for sta in selected_stations[net].keys():
                    for compo in selected_stations[net][sta]:
                        if compo not in compo_list:
                            remaining_stations[net][sta].pop(compo)

        if dset_name:
            # creation of dataset names:
            dataset_names = []
            for net in remaining_stations.keys():
                for sta in remaining_stations[net].keys():
                    for compo in remaining_stations[net][sta]:
                        dset = "/" + net + "/" + sta + "/" + compo
                        dataset_names.append(dset)
            return dataset_names
        else:
            return selected_stations

    def get_stream(self, net_list=None, sta_list=None, compo_list=None):
        """

        :param net_list:
        :param sta_list:
        :param compo_list:
        :return:
        """

        selected_stations = self.create_dataset_names(net_list=net_list, sta_list=sta_list, compo_list=compo_list)

        final_stream = Stream()
        for net in selected_stations.keys():
            for sta in selected_stations[net].keys():
                for compo in selected_stations[net][sta]:
                    final_stream.append(self.get_trace(net, sta, compo))

        return final_stream

    def remove_instrument_response_smartsolo_5hz(self, output_folder):
        return -1

    def plot_trace(self, net, sta, compo, *args, **kwargs):
        """

        :param net:
        :param sta:
        :param compo:
        :param args:
        :param kwargs:
        :return:
        """
        selected_trace = self.get_trace(net, sta, compo)
        selected_trace.plot(*args, **kwargs)

    def to_eqtransformer_format(self, output_folder):
        """
        Output data need to be sampled at 100hz.
        """
        year = self.day.year
        julian_day = self.day.julday
        for net, stations in self.station_list.items():
            for sta, compo_list in stations.items():
                for compo in compo_list:
                    # Get trace for current network, station and component:
                    current_trace = self.get_trace(net, sta, compo)
                    current_trace.data = current_trace.data.astype(np.float32)
                    current_trace.resample(100.)
                    current_filename = make_eqtransformer_name(current_trace.stats, year, julian_day)
                    full_output = os.path.join(output_folder + current_filename)
                    # save data into folder:
                    if not os.path.isdir(os.path.dirname(full_output)):
                        os.makedirs(os.path.dirname(full_output))
                    current_trace.write(full_output, format="MSEED")

    def to_sds(self, output_folder, verbose=False):
        """

        :param output_folder:
        :param verbose:
        :return:
        """
        year = self.day.year
        julian_day = self.day.julday
        for net, stations in self.station_list.items():
            for sta, compo_list in stations.items():
                for compo in compo_list:
                    # Get trace for current network, station and component:
                    current_trace = self.get_trace(net, sta, compo)
                    current_trace.data = current_trace.data.astype(np.float32)
                    # Create full SDS folder and filename:
                    sds_filename = make_sds_name(current_trace.stats, year, julian_day)
                    full_output = os.path.join(output_folder, sds_filename)
                    # save data into SDS folder:
                    if not os.path.isdir(os.path.dirname(full_output)):
                        os.makedirs(os.path.dirname(full_output))
                    current_trace.write(full_output, format="MSEED", encoding='FLOAT32')


def make_sds_name(stats, year, jday):
    sds = os.path.join("YEAR", "NET", "STA", "CHAN.TYPE", "NET.STA.LOC.CHAN.TYPE.YEAR.JDAY")
    filename = sds.replace('YEAR', "%04i" % year)
    filename = filename.replace('NET', stats.network)
    filename = filename.replace('STA', stats.station)
    filename = filename.replace('LOC', stats.location)
    filename = filename.replace('CHAN', stats.channel)
    filename = filename.replace('JDAY', "%03i" % jday)
    filename = filename.replace('TYPE', "D")
    return filename


def make_eqtransformer_name(stats, year, day):
    sds = os.path.join("STA", "NET.STA.LOC.CHAN__date1Thr1Z__date2Thr2Z.mseed")
    date1 = UTCDateTime(str(year) + str(day).zfill(3)).strftime("%Y%m%d")
    date2 = UTCDateTime(str(year) + str(day).zfill(3)).strftime("%Y%m%d")
    hr1 = str(stats.starttime.hour).zfill(2) + str(stats.starttime.minute).zfill(2) + str(stats.starttime.second).zfill(2)
    hr2 = str(stats.endtime.hour).zfill(2) + str(stats.endtime.minute).zfill(2) + str(stats.endtime.second).zfill(2)
    filename = sds.replace('NET', stats.network)
    filename = filename.replace('STA', stats.station)
    filename = filename.replace('LOC', stats.location)
    filename = filename.replace('CHAN', stats.channel)
    filename = filename.replace('date1', date1)
    filename = filename.replace('date2', date2)
    filename = filename.replace('hr1', hr1)
    filename = filename.replace('hr2', hr2)
    return filename



"""
#-- example of main script using the functions defined above
# FL: comment this out to avoid having hard-coded main programs within modules
if __name__ == '__main__':
    data_path = "E:/2019_KIRUNA/data_100.0hz/daily/DAM/2019/day_176.h5"

    d = SisDailyDatah5(data_path)
    st = d.get_stream()
    date = st[0].stats.starttime
    st.trim(starttime=date + 8 * 60 * 60, endtime=date + 13 * 60 * 60)

    print("END OF SCRIPT")
"""


